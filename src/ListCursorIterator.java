import java.util.*;

/**
 * 連結リスト(ListCell)のイテレータ
 */
public class ListCursorIterator<E> implements ListIterator<E>{
	ListCursor mList;
	int current; //	現在注目しているノード
	int index;
	public static int NULL = -1;
	
	ListCursorIterator(ListCursor<E> list){
		mList = list;
		current = mList.head;
		index = 0;
	}
	// 要素を追加する
	public void add(E e){
		mList.add((E)e,current);
	}
	// 現在の要素にデータをセットする
	public void set(Object e){
		mList.mData[current].data = (E)e;
	}
	// 次の要素があるか
	public boolean hasNext(){
		return mList.mData[current].next != NULL;
	}
	
	// 次の要素を返す
	public E next(){
		if (mList.mData[current].next == NULL)
			throw new NoSuchElementException();
		current = mList.mData[current].next;
		index++;
		return (E)mList.mData[current].data;
	}
	// 次の要素のインデックスを返す
	public int nextIndex(){
		return index+1;
	}
	// 前の要素があるか
	public boolean hasPrevious(){
		if (mList.parent(current)!=NULL)
			return true;
		else
			return false;
	}
	// 前の要素を返す
	public E previous(){
		if (index==0)
			throw new NoSuchElementException();
		current = mList.parent(current);
		index--;
		return (E)mList.mData[current].data;
	}
	//前の要素のインデックスを返す
	public int previousIndex(){
		return index-1;
	}
	// 現在の要素を削除する
	public void remove(){
		mList.remove(current);
	}
}

