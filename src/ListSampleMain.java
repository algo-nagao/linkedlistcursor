import java.util.*;
public class ListSampleMain {

	public static void main(String[] args) {
		ListCursor<Integer> list = new ListCursor<Integer>(10);
		list.addLast(10);
		list.addLast(20);
		list.addFirst(30);
		list.addLast(40);
		list.addLast(50);
		System.out.println("List:"+list.toString());
		
		// イテレータを生成
		ListIterator iter = list.iterator();
		int count = 1;
		while(iter.hasNext()){
			System.out.println(count++ + "番目:"+iter.next());
		}
	}

}
