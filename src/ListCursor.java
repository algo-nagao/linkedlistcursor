//リストセルによる線形リストの実装
//Iterator による操作に対応

class Cell<E>{
	E data;			// データ
	int next;	// 次のデータへのリンク（カーソル）
	private static final int NULL = -1;

	Cell(){
		this.data = null;
		this.next = NULL;
	}
	void set(E data, int next){
		this.data = data;
		this.next = next;
	}
}

public class ListCursor<E> {
	// リストヘッダ
	Cell<E>[] mData;	// List 本体
	private int size;
	int head;
	private static final int NULL = -1;
	/* コンストラクタ
	 */
	ListCursor(int max){
		mData = new Cell[max];
		for (int i=0 ; i<max; i++){
			mData[i] = new Cell<E>();
		}
		size = max;
		head = 0;
	}

	/**
	 * 空き領域のインデックスを返す
	 * @return
	 */
	private int getFreeCell(){
		for (int i=1 ; i<size; i++){
			if (mData[i].data==null)
				return i;
		}
		return NULL;		
	}

	/**
	 * リストの先頭に、指定された要素を追加します。
	 * @param e
	 */
	public void addFirst(E e){
		add(e,head);
	}
	/**
	 * リストの最後に、指定された要素を追加します。
	 * @param e
	 */
	public void addLast(E e){
		add(e,tail());
	}
	/**
	 * リストの指定された位置に、指定された要素を追加します。
	 * @param e 要素 index 親セル
	 */
	public void add(E e,int index){
		int p = getFreeCell();	// 空きリスト
		mData[p].set(e,mData[index].next);
		mData[index].next = p;
	}

	/**
	 * リストの長さを返す
	 */
	public int size(){
		int p = head;
		int n = 0;
		while (mData[p].next != NULL){
			p = mData[p].next;
			n++;
		}
		return n;
	}
	/**
	 * 指定されたインデックスのデータを削除
	 * @param index
	 * @return 成功したか
	 */
	public boolean remove(int target){
		// TODO Free に
		int prev = parent(target);
		if (prev==NULL)
			return false;
		mData[prev].next = mData[target].next;
		mData[target].data = null;
		return true;
	}
	/**
	 * nodeの親を返す
	 * @param cell
	 * @return
	 */
	public int parent(int cell){
		int p = head;
		while(mData[p].next!=NULL){
			if(mData[p].next==cell)
				return p;
			p = mData[p].next;
		}
		return NULL;
	}
	/**
	 * nodeの子を返す
	 * @param cell
	 * @return
	 */
	public int child(int cell){
		return mData[cell].next;
	}
	/**
	 * リストに指定された要素がある場合にtrueを返す
	 * @param element
	 * @return 
	 */
	public int search(E element){
		int p = mData[head].next;
		while(p!=NULL){
			if (mData[p].data == element)
				return p;
			p = mData[p].next;
		}
		return NULL;
	}

	/**
	 * リストの最後の要素を返す
	 * @param element
	 * @return 
	 */
	public int tail(){
		int  p = head;
		while(mData[p].next!=NULL){
			p = mData[p].next;
		}
		return p;
	}
	/**
	 * リストの内容を文字列として返す
	 */
	public String toString(){
		String buf = "[";
		int p = head;
		while(mData[p].next!=NULL){
			p = mData[p].next;
			buf += mData[p].data.toString() + " ";
		}buf +="]";
		return buf;
	}
	/**
	 * リストのイテレータを得る
	 * @return イテレータ
	 */
	public ListCursorIterator<E> iterator(){
		return new ListCursorIterator<E>(this);
	}
}
